<div class="form-group {{ $errors->has('kategori') ?
    ' has-error': '' }}">
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif 
</div>
<div class="form-group">
	<label class="col-sm-2">Deskripsi</label>
	<div class="col-sm-9">
		{!! Form::text('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi"]) !!}
        {!! $errors->first{'kategori','<span class="help-block">:message</span>' !!}}
	</div>
</div>
