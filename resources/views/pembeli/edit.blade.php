@extends('master')
@section('content')

<!-- <div class="form-group">
<center>
<label class="col-sm-2 control-label">Data Pembeli</label>
<div class="form-group">
<div class="col-sm-5">
{!! Form::text('nama',null,['class'=>'form-control','placeholder'=>"Nama"]) !!}
</div>
</div>

<div class="form-group">
<div class="col-sm-5">
{!! Form::text('notlp',null,['class'=>'form-control','placeholder'=>"No Telepon"]) !!}
</div>
</div>

<div class="form-group">
<div class="col-sm-5">
{!! Form::text('email',null,['class'=>'form-control','placeholder'=>"Email"]) !!}
</div>
</div>

<div class="form-group">
<div class="col-sm-5">
{!! Form::text('alamat',null,['class'=>'form-control','placeholder'=>"Alamat"]) !!}
</div>
</div>

<div class="form-group">
<div class="col-sm-5">
{!! Form::text('username',null,['class'=>'form-control','placeholder'=>"Username"]) !!}
</div>
</div>

<div class="form-group">
<div class="col-sm-5">
{!! Form::text('password',null,['class'=>'form-control','placeholder'=>"Password"]) !!}
</div>
</div>
</center>
</div>
 --><div class="panel panel-primary">
		
		{!! Form::model($pembeli,['url'=>'pembeli/update/'.$pembeli->id,'class'=>'form-horizontal']) !!}
		@include('pembeli.form')

		<div style="width:100%;text-align:center;">
			<button class="btn btn-primary"><i class="fa fa-save"></i>
			Simpan</button>
			<input type="button" value="Reset" class="btn btn-danger" onClick="window.location.reload()"/>
		</div>
	{!! Form::close() !!}
	</div>
@endsection