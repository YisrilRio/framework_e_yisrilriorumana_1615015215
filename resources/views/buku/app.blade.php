@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-default">
<div class="panel-heading">
<strong>Data Buku</strong>
<div class="pull-right">
Tambah Data <a href="{{url('tambah/buku')}}"><img src="{{ asset('add.ico') }}" height="20"></img></a>
</div>
<div class="penel-body">
<table class="table">
<tr>
<td> Judul </td>
<td> Kategori </td>
<td> Penerbit </td>
<td> Penulis </td>
<td> Aksi </td>
</tr>
@foreach($datac as $buku)
<tr>
<td>{{ $buku->judul }}</td>
<td>{{ $buku->kategori->deskripsi or 'kosong'}}</td>
<td>{{ $buku->penerbit }}</td>
<td>{{ $buku->penulis->first()->nama or 'kosong'}}</td>
<td>
<a href="{{url('buku/edit/'.$buku->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>
<a href="{{url('buku/hapus/'.$buku->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
</div>
@endsection