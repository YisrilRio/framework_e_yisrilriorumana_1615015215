<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penulis extends Model
{
    protected $table = 'penulis';
    protected $fillable = [
    	'nama',
    	'notlp',
    	'email',
    	'alamat',
    ];

    public function buku_penulis(){
		return $this->hasOne(buku_penulis::class);
	}

    public function buku(){
        return $this->belongsToMany(buku::class);
    }
}
