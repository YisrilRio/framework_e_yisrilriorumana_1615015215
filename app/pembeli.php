<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pembeli extends Model
{
    protected $table = 'pembeli';
    protected $fillable = [
    'nama',
    'noltp',
    'email',
    'alamat',
    'pengguna_id',
	];

	// public function getUsernameAttribute()
	// {
	// 	Return $this->pengguna->username;
	// }
	
	// public function getPasswordAttribute()
	// {
	// 	Return $this->pengguna->password;
	// }
	
	public function buku_pembeli(){
		return $this->hasOne(buku_pembeli::class);
	}

	public function Pengguna(){
		return $this->belongsTo(Pengguna::class);
	}
}
