<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $table = 'admin';
    protected $fillable = [
    	'nama',
    	'notlp',
    	'email',
    	'alamat',
    	'pengguna_id',
    ];

    public function Pengguna(){
    	return $this->belongsTo(Pengguna::class);
    }
}
