<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku_penulis extends Model
{
    protected $table = 'buku_penulis';
    protected $fillable = [
    	'penulis_id',
    	'buku_id',
    ];

    public function buku(){
		return $this->belongsTo(buku::class);
	}

    public function penulis(){
		return $this->belongsTo(penulis::class);
	}
}
