<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku extends Model
{
    protected $table = 'buku';
    protected $fillable = [
    	'judul',
    	'kategori_id',
    	'penerbit',
    	'tanggal',
    ];

    public function kategori(){
        return $this->belongsTo(kategori::class);
    }

    public function buku_pembeli(){
        return $this->hasOne(buku_pembeli::class);
    }

    public function buku_penulis(){
        return $this->hasOne(buku_penulis::class);
    }

    public function penulis(){
        return $this->belongsToMany(penulis::class);
    }
}