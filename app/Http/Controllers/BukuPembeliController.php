<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BukuPembeliController extends Controller
{
    public function awal(){
    	$buku_pembeli = BukuPembeli::all();
    	return view('buku_pembeli.app',compact('buku_pembeli'));
    }

    public function tambah(){
    	return view('pembeli.tambah');
    }
    public function simpan(Request $input){
    	$buku_pembeli= new BukuPembeliController();
    	$buku_pembeli->nama = $input->nama;
    	$buku_pembeli->notlp = $input->notlp;
    	$buku_pembeli->email = $input->email;
    	$buku_pembeli->alamat = $input->alamat;
    	$status->$buku_pembeli->save();
    	return redirect('buku_pembeli');
    }
    public function edit($id){
    	$buku_pembeli = BukuPembeli::find($id);
    	return view('pembeli.edit')->with(array('buku_pembeli'=>$buku_pembeli));
    }
    public function update($id){
    	$buku_pembeli = BukuPembeli::find($id);
    	$buku_pembeli->nama = $input->nama;
    	$buku_pembeli->notlp = $input->notlp;
    	$buku_pembeli->email = $input->email;
    	$buku_pembeli->alamat = $input->alamat;
    	$status->$buku_pembeli->save();
    	return redirect('buku_pembeli')->with(['status'=>$status]);
    }
     public function hapus($id){
    	$buku_pembeli = BukuPembeli::find($id);
    	$buku_pembeli->delete();
    	return redirect('buku_pembeli');
    }
}
