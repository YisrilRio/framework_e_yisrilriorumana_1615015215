<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pembeli;
use App\Pengguna;

class PembeliController extends Controller
{
    public function awal()
    {
    	$pembeli= pembeli::all();
    	return view('pembeli.app',compact('pembeli'));
    }
    public function tambah()
    {
    	return view('pembeli.tambah');
    }
    public function simpan(Request $input)
    {
    $this->validate($input,[
        'nama'     => 'required',
        'notlp'    => 'required',
        'email'    => 'required',
        'alamat'   => 'required',
        'username' => 'required',
        'password' => 'required',
    ]);
        $pengguna = new Pengguna();
        $pengguna->username = $input->username;
        $pengguna->password = $input->password;
        $pengguna->level = "village";
        $pengguna->save();

    	$pembeli = new pembeli();
    	$pembeli->nama = $input->nama;
    	$pembeli->notlp = $input->notlp;
    	$pembeli->email = $input->email;
    	$pembeli->alamat = $input->alamat;
    	$status = $pembeli->save();
    	return redirect('pembeli')->with(['status'=>$status]);
    }
    public function edit($id)
    {
        $pembeli = pembeli::find($id);
        return view('pembeli.edit')->with(array('pembeli'=>$pembeli));
    }
    public function update($id, Request $input)
    {
        $pembeli = pembeli::find($id);
        $pembeli->nama = $input->nama;
        $pembeli->notlp = $input->notlp;
        $pembeli->email = $input->email;
        $pembeli->alamat = $input->alamat;
        $status = $pembeli->save();
        return redirect('pembeli')->with(['status'=>$status]);
    }
    public function hapus($id)
    {
    	$pembeli = pembeli::find($id);
    	$pembeli->delete();
    	return redirect('pembeli');
    }

}