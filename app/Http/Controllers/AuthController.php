<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Auth\SessionGuard;
use App\Pengguna;
use Auth;

class AuthController extends Controller
{
	public function index()
	{
		return view('master');
	}

	public function formLogin()
	{
		if(Auth::check())
			{
				return redirect('/');
			}
		return view('login');
	}

	public function proses(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required',
		]);
		$pengguna =
		Pengguna::where($request->only('username','password'))->first();
		if (!is_null($pengguna))
			{
			Auth::login($pengguna);
				if(Auth::check())
				return redirect('/')->with('informasi',"Selamat Datang ".Auth::user()->username);
			}
		return redirect()->back()->withErrors(['Pengguna Tidak Ditemukan']);
	}

	public function logout()
	{
		if(Auth::check())
			{
				Auth::logout();
				return redirect('/login')->withErrors(['Silahkan Login Untuk Masuk Kesistem']);
			}
		else
		{
			return redirect('/login')->withErrors(['Silahkan Login Terlebih Dahulu']);
		}
	}
}