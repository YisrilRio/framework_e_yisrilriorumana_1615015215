<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Kategori;

class KategoriController extends Controller
{
    public function awal(){
    	$kategori = Kategori::all();
    	return view('kategori.kategori',compact('kategori'));
    }
    public function tambah(){
        $kategori = Kategori::all();
        return view('kategori.tambah', compact('kategori'));
    }
    public function simpan(Request $input){
    	$this->validate($input,
            [
                'deskripsi' => 'required|min:3',
            ]);
        $kategori = new Kategori();
    	$kategori->deskripsi = $input->deskripsi;
    	$kategori->save();
    	return redirect('kategori');
    }
    public function edit($id){
    	$kategori = Kategori::find($id);
    	return view('kategori.edit')->with(array('kategori'=>$kategori));
    }
    public function update($id, Request $input){
    	$kategori = Kategori::find($id);
    	$kategori->deskripsi = $input->deskripsi;
    	$kategori->save();
    	return redirect('kategori');
    }
     public function hapus($id){
    	$penulis = Kategori::find($id);
    	$penulis->delete();
    	return redirect('kategori');
    }
}