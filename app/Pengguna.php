<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Authenticatable
{

    protected $table = 'pengguna';
    protected $fillable = 
    [
    	'username',
    	'password',
    ];

    protected $hidden = 
    [
    		'password',
    		'remember_token',
    ];

    public function admin(){
            return $this->hasOne(admin::class);
    }

    public function Pembeli(){
    	return $this->hasOne(Pembeli::class);
    }
}