<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tes', function () {
    return view('tes');
});

Route::get('pengguna', 'PenggunaController@awal');
Route::get('/pengguna/simpan', 'PenggunaController@simpan');

Route::get('/buku',"BukuController@awal");
Route::get('/buku/tambah',"BukuController@tambah");
Route::get('/tambah',"BukuController@lihat");
Route::get('/tambah',"BukuController@lihat");

Route::get('/tambah/penulis',"PenulisController@tambah");
Route::post('/penulis/simpan',"PenulisController@simpan");
Route::get('/penulis',"PenulisController@awal");
Route::get('/penulis/edit/{penulis}',"PenulisController@edit");
Route::post('/penulis/update/{penulis}',"PenulisController@update");
Route::get('/penulis/hapus/{penulis}',"PenulisController@hapus");

Route::get('/tambah/buku',"BukuController@tambah");
Route::post('/buku/simpan',"BukuController@simpan");

Route::get('/kategori',"KategoriController@awal");
Route::get('/kategori/tambah',"KategoriController@tambah");
Route::post('/kategori/simpan',"KategoriController@simpan");
Route::get('/kategori/edit/{kategori}',"KategoriController@edit");
Route::post('/kategori/update/{kategori}',"KategoriController@update");
Route::get('/kategori/hapus/{kategori}',"KategoriController@hapus");


Route::get('/pembeli/tambah',"PembeliController@tambah");
Route::post('/pembeli/simpan',"PembeliController@simpan");
Route::get('/pembeli',"PembeliController@awal");
Route::get('/pembeli/edit/{buku_pembeli}',"PembeliController@edit");
Route::post('/pembeli/update/{buku_pembeli}',"PembeliController@update");
Route::get('/pembeli/hapus/{buku_pembeli}',"PembeliController@hapus");

Route::get('/admin',"AdminController@awal");
Route::get('/admin/tambah',"AdminController@tambah");
Route::post('/admin/simpan',"AdminController@simpan");
Route::get('/admin/lihat',"AdminController@lihat");
Route::get('/admin/edit/{admin}',"AdminController@edit");
Route::post('/admin/update/{admin}',"AdminController@update");
Route::get('/admin/hapus/{admin}',"AdminController@hapus");
Route::get('/','AuthController@index');
Route::get('/login','AuthController@formLogin');
Route::post('/login','AuthController@proses');
Route::get('/logout','AuthController@logout');